#!r6rs
(library (term)
  (export t= t* t/ monomial->term number->term)
  (import (rnrs base)
	  (variables)
	  (monomial))

  (define (t= t u)
    (and (equal? (car t) (car u))
	 (= (cdr t) (cdr u))))
  
  (define (t* t u)
    (cons (m* (car t) (car u))
	  (* (cdr t) (cdr u))))
  
  (define (t/ t u)
    (cons (m/ (car t) (car u))
	  (/ (cdr t) (cdr u))))

  (define (number->term n)
    (cons (make-vector (string-length (variables)) 0) n))
  
  (define (monomial->term m)
    (cons m 1))
  )
