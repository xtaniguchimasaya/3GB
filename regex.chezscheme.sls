#!r6rs
(library (regex)
  (export regex-replace regex-replace-all)
  (import (chezscheme))
  
  (define regex
    (load-shared-object "./regex.dylib"))
  
  (define regex-replace
    (foreign-procedure
     "regex_replace"
     (string string string int) scheme-object))
  
  (define (regex-replace-all s e fmt)
    (regex-replace s e fmt 0))
  )
